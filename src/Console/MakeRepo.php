<?php

namespace Tarre\RepositoryMaker\Console;


use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Tarre\RepositoryMaker\Exceptions\CouldNotLoadConfigurationFileException;
use Tarre\RepositoryMaker\Exceptions\FailedToGenerateException;
use Tarre\RepositoryMaker\Exceptions\InvalidFormatterException;
use Tarre\RepositoryMaker\Exceptions\InvalidRelatedModelException;
use Tarre\RepositoryMaker\Exceptions\InvalidRepositoryNameException;
use Tarre\RepositoryMaker\Generator;

/**
 * @property \Illuminate\Console\OutputStyle public_output
 */
class MakeRepo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:repository {name?} {relatedModel?} {--controller} {--route=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Repository pattern';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws InvalidRepositoryNameException
     * @throws InvalidRelatedModelException
     */
    public function handle()
    {

        $relatedModel = $this->argument('relatedModel');
        $name = $this->argument('name');

        if (!$name) {
            $name = $this->ask('Enter the name of the Repository');
        }

        if (empty($name)) {
            throw new InvalidRepositoryNameException;
        }

        if (!$relatedModel) {
            $relatedModel = $this->ask('Enter the namespace of the related model. Example: App\\User');
        }

        if (empty($relatedModel)) {
            throw new InvalidRelatedModelException;
        }

        if (!ctype_upper(substr($name, 0, 1))) {
            if ($this->confirm('The given name\'s first letter is not upper-cased, do you want to uppercase the first letter?', true)) {
                $name = ucfirst($name);
            }
        }


        $this->comment('Trying to create repository: ' . $name);


        try {
            $generator = new Generator($name, $relatedModel, $this->option('controller'), $this->option('route'));
        } catch (CouldNotLoadConfigurationFileException $e) {
            $this->error('Could not load the configuration file');
            if ($this->ask('Do you want to run "vendor:publish --tag=laravel-repository-maker --force" ?')) {
                $this->call('vendor:publish', [
                    '--tag' => 'laravel-repository-maker',
                    '--force' => true
                ]);
            }
        } catch (FailedToGenerateException $e) {
            $this->warn('Failed to generate repository...: ' . $e->getMessage());
        }

        try {
            $generator->make();
            foreach ($generator->getMessages() as $message) {
                $this->comment($message);
            }
            $this->comment('Successfully created repository');
        } catch (\Exception $e) {
            $this->warn($e->getMessage());
        }

    }
}
