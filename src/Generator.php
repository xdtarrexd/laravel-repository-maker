<?php
/**
 * Created by PhpStorm.
 * User: Tarre
 * Date: 2018-07-06
 * Time: 13:25
 */

namespace Tarre\RepositoryMaker;


use Illuminate\Support\Facades\Config;
use Tarre\RepositoryMaker\Exceptions\CouldNotLoadConfigurationFileException;
use Tarre\RepositoryMaker\Exceptions\FailedToGenerateException;
use Tarre\RepositoryMaker\Exceptions\FileAlreadyExistsException;
use Tarre\RepositoryMaker\Exceptions\InvalidArrayException;
use Tarre\RepositoryMaker\Exceptions\InvalidFormatterException;
use Tarre\RepositoryMaker\Exceptions\InvalidRelatedModelException;
use Tarre\RepositoryMaker\Exceptions\InvalidRouterNameException;

class Generator
{
    protected $name;
    protected $relatedModel;
    protected $controller;
    protected $router;
    protected $config = [];
    protected $messages = [];

    /**
     * Generator constructor.
     * @param string $name
     * @param null $relatedModel
     * @throws FailedToGenerateException
     * @throws CouldNotLoadConfigurationFileException
     */
    public function __construct(string $name, $relatedModel = null, $controller = false, $router = false)
    {

        if (!Config::has('laravel-repository-maker')) {
            throw new CouldNotLoadConfigurationFileException;
        }

        $this->config = Config::get('laravel-repository-maker');
        $this->name = $name;
        $this->controller = $controller;
        $this->router = $router;
        $this->relatedModel = $relatedModel;

        // Validate input and configuration
        try {
            $this->validateInput();
        } catch (\Exception $e) {
            throw new FailedToGenerateException($e->getMessage());
        }

    }

    /**
     * @throws InvalidRelatedModelException
     * @throws CouldNotLoadConfigurationFileException
     * @throws InvalidFormatterException
     * @throws InvalidArrayException
     */
    protected function validateInput()
    {
        if ($this->hasRelatedModel() && !$this->hasValidRelatedModel()) {
            throw new InvalidRelatedModelException('The given related model is invalid, please make sure you use double slashed namespaces. Example "App\\\\MyTestModel" also check if the model exists.');
        }

        if (
            !is_array($this->config['formatter']) ||
            !is_array($this->config['interface']) ||
            !is_array($this->config['interface']['extends']) ||
            !is_array($this->config['uses']) ||
            !is_array($this->config['class']) ||
            !is_array($this->config['class']['extends']) ||
            !is_array($this->config['class']['implements']) ||
            !is_array($this->config['class']['uses'])
        ) {
            throw new InvalidArrayException('The config keys: "formatter", "interface", "interface.extends", "uses" "class", "class.extends", "class.implements" and "class.uses" must be the type "array"');
        }

        if (
            !$this->config['formatter']['method_variables'] instanceof \Closure ||
            !$this->config['formatter']['repository_names'] instanceof \Closure ||
            !$this->config['formatter']['controller_name'] instanceof \Closure ||
            !$this->config['formatter']['class_model_attribute'] instanceof \Closure
        ) {
            throw new InvalidFormatterException('The config keys: "formatter.method_variables" and "formatter.repository_names" must be the type "Closure"');
        }

    }

    protected function hasRelatedModel()
    {
        return !is_null($this->relatedModel);
    }

    protected function hasValidRelatedModel()
    {
        return class_exists($this->relatedModel);
    }

    protected function interfaceCanExtend()
    {
        return count($this->config['interface']['extends']) > 0;
    }

    protected function classCanExtend()
    {
        return count($this->config['class']['extends']) > 0;
    }

    protected function classCanUse()
    {
        return count($this->config['class']['uses']) > 0;
    }

    protected function classCanImplement()
    {
        return count($this->config['class']['implements']) > 0;
    }

    protected function formatter($section, $value)
    {
        return $this->config['formatter'][$section]($value);
    }

    /**
     * @return bool
     * @throws FileAlreadyExistsException
     * @throws InvalidRouterNameException
     */
    public function make()
    {
        $interface = $this->makeInterface();
        $class = $this->makeClass($interface);
        if ($this->controller) {
            $controller = $this->makeController($interface);
        }
        $this->registerToProvider($interface, $class);


        if ($this->router && $this->controller) {
            $this->registerToRouter($this->router, $controller);
        }
    }

    public function getMessages(): array
    {
        return $this->messages;
    }

    /**
     * @throws FileAlreadyExistsException
     */
    protected function makeInterface()
    {
        $desiredName = $this->formatter('repository_names', str_replace('%name%', $this->name, $this->config['names']['interface']));
        $targetNamespaceName = $this->formatter('repository_names', $this->name);

        $source = $this->config['template_path'] . DIRECTORY_SEPARATOR . 'interface.template';
        $target = sprintf('%s' . DIRECTORY_SEPARATOR . '%s' . DIRECTORY_SEPARATOR . '%s.php', $this->config['output_path'], $targetNamespaceName, $desiredName);

        $idAttributeName = $this->name . '_id';
        $modelNamespace = preg_replace('/(.*)\\\\.*/m', '$1', $this->relatedModel);
        $modelClassName = class_basename($this->relatedModel);

        if ($this->interfaceCanExtend()) {
            $extends = 'extends ' . collect($this->config['interface']['extends'])
                    ->map(function ($class) {
                        return class_basename($class);
                    })->implode(', ');
        } else {
            $extends = '';
        }

        $uses = collect($this->config['uses'])
            ->merge($this->config['interface']['extends'])
            ->map(function ($uses) {
                return sprintf('use %s;', $uses);
            })->implode("\n");

        $namespace = sprintf('%s\%s', $this->config['repository_namespace'], $targetNamespaceName);
        $variableNameId = $this->formatter('method_variables', $idAttributeName);

        $macroTable = [
            '%InterfaceName%' => $desiredName,
            '%namespace%' => $namespace,
            '%variableNameId%' => $variableNameId,
            '%relatedModelNamespace%' => $modelNamespace,
            '%relatedModel%' => $modelClassName,
            '%extends%' => $extends,
            '%uses%' => $uses
        ];

        $this->compileTemplate($source, $target, $macroTable);
        return sprintf('%s\%s', $namespace, $desiredName);
    }

    /**
     * @param $interFaceClass
     * @return string
     * @throws FileAlreadyExistsException
     */
    protected function makeClass($interFaceClass)
    {
        // implement class
        $this->config['class']['implements'][] = $interFaceClass;

        $desiredName = $this->formatter('repository_names', str_replace('%name%', $this->name, $this->config['names']['class']));
        $targetNamespaceName = $this->formatter('repository_names', $this->name);
        $relatedModelAttribute = $this->formatter('class_model_attribute', str_replace('%name%', $this->name, $this->config['names']['class_model_attribute']));

        $source = $this->config['template_path'] . DIRECTORY_SEPARATOR . 'class.template';
        $target = sprintf('%s' . DIRECTORY_SEPARATOR . '%s' . DIRECTORY_SEPARATOR . '%s.php', $this->config['output_path'], $targetNamespaceName, $desiredName);

        $idAttributeName = $this->name . '_id';
        $modelNamespace = preg_replace('/(.*)\\\\.*/m', '$1', $this->relatedModel);
        $modelClassName = class_basename($this->relatedModel);

        if ($this->classCanExtend()) {
            $extends = 'extends ' . collect($this->config['class']['extends'])
                    ->map(function ($class) {
                        return class_basename($class);
                    })->implode(', ');
        } else {
            $extends = '';
        }

        if ($this->classCanImplement()) {
            $implements = 'implements ' . collect($this->config['class']['implements'])
                    ->map(function ($class) {
                        return class_basename($class);
                    })->implode(', ');
        } else {
            $implements = '';
        }

        $uses = collect($this->config['uses'])
            ->merge($this->config['class']['extends'])
            ->merge($this->config['class']['uses'])
            ->merge($this->config['class']['implements'])
            ->map(function ($uses) {
                return sprintf('use %s;', $uses);
            })->implode("\n");

        $namespace = sprintf('%s\%s', $this->config['repository_namespace'], $targetNamespaceName);
        $variableNameId = $this->formatter('method_variables', $idAttributeName);


        $macroTable = [
            '%ClassName%' => $desiredName,
            '%namespace%' => $namespace,
            '%implements%' => $implements,
            '%variableNameId%' => $variableNameId,
            '%relatedModelNamespace%' => $modelNamespace,
            '%relatedModel%' => $modelClassName,
            '%extends%' => $extends,
            '%uses%' => $uses,
            '%traits%' => '',
            '%relatedModelAttribute%' => $relatedModelAttribute
        ];

        $this->compileTemplate($source, $target, $macroTable);
        return sprintf('%s\%s', $namespace, $desiredName);
    }

    /**
     * @param $interfaceClass
     * @throws FileAlreadyExistsException
     */
    protected function makeController($interfaceClass)
    {

        $desiredName = $this->formatter('controller_name', str_replace('%name%', $this->name, $this->config['names']['controller']));
        $source = $this->config['template_path'] . DIRECTORY_SEPARATOR . 'controller.template';
        $target = app_path(sprintf('Http' . DIRECTORY_SEPARATOR . 'Controllers' . DIRECTORY_SEPARATOR . '%s.php', $desiredName));
        $relatedModelAttribute = $this->formatter('class_model_attribute', str_replace('%name%', $this->name, $this->config['names']['class_model_attribute']));
        $modelClassName = class_basename($this->relatedModel);

        $macroTable = [
            '%repositoryNamespace%' => $interfaceClass,
            '%repositoryInterface%' => class_basename($interfaceClass),
            '%ControllerName%' => $desiredName,
            '%relatedModelAttribute%' => $relatedModelAttribute,
            '%relatedModel%' => $modelClassName
        ];

        $this->compileTemplate($source, $target, $macroTable);

        return $desiredName;
    }

    protected function registerToProvider($interface, $class)
    {
        $file = app_path('Providers' . DIRECTORY_SEPARATOR . 'AppServiceProvider.php');
        $handle = fopen($file, 'rbw+');
        $content = fread($handle, filesize($file));

        // Uses
        if (preg_match_all('/use\h([^;]+)/', $content, $matches, 1)) {
            $uses = collect($matches[1])
                ->merge([$interface, $class])
                ->unique()
                ->sort()
                ->map(function ($value) {
                    return sprintf('use %s;', $value);
                })->implode("\n");
        } else {
            $uses = '';
        }

        // Registers
        if (preg_match_all('/\$this->app->bind\(([^,]+),\h*([^)]+).*/', $content, $matches, 2)) {
            $registers = collect($matches)
                ->map(function ($arr) {
                    return [
                        'interface' => $arr[1],
                        'class' => $arr[2]
                    ];
                })
                ->merge([
                    [
                        'interface' => class_basename($interface) . '::class',
                        'class' => class_basename($class) . '::class'
                    ]
                ])
                ->unique()
                ->sortBy('interface')
                ->map(function ($binder) {
                    return sprintf('        $this->app->bind(%s, %s);', $binder['interface'], $binder['class']);
                })->implode("\n");
        } else {
            $registers = sprintf('        $this->app->bind(%s, %s);', class_basename($interface) . '::class', class_basename($class) . '::class');
        }

        // replace with new content
        $content = preg_replace('/(\/\/\hUSES-START)\n[^\/]*\n(\/\/\hUSES-STOP)/', "$1\n" . $uses . "\n$2", $content);
        $content = preg_replace('/(\h*\/\/\hREGISTER-START)\n*[^\/]*\n(\h*\/\/\hREGISTER-STOP)/', "$1\n" . $registers . "\n$2", $content);

        // rewind to start and write new content
        rewind($handle);
        fwrite($handle, $content);
        $this->messages[] = sprintf('Modified: ' . $file);
    }

    /**
     * @param $router
     * @param $controller
     * @throws InvalidRouterNameException
     */
    protected function registerToRouter($router, $controller)
    {
        $file = base_path('routes' . DIRECTORY_SEPARATOR . $router . '.php');
        if (!file_exists($file)) {
            throw new InvalidRouterNameException('The route ' . $router . ' does not exist.');
        }
        $targetHandle = fopen($file, 'a');
        fwrite($targetHandle, sprintf("\nRoute::resource('%s', '%s');\n", $this->formatter('router_resource_name', $this->name), $controller));
        fclose($targetHandle);
        $this->messages[] = sprintf('Modified: ' . $file);
    }

    /**
     * @param $source
     * @param $target
     * @param array $macroTable
     * @throws FileAlreadyExistsException
     */
    protected function compileTemplate($source, $target, array $macroTable)
    {
        if (file_exists($target)) {
            throw new FileAlreadyExistsException(sprintf('The file "%s" already exists.', $target));
        }

        // Create dir if needed
        if (!file_exists(dirname($target))) {
            mkdir(dirname($target), 0777, true);
        }

        $sourceHandle = fopen($source, 'rb');
        $sourceContent = fread($sourceHandle, filesize($source));
        fclose($sourceHandle);

        $targetContent = str_replace(array_keys($macroTable), $macroTable, $sourceContent);


        $targetHandle = fopen($target, 'w+');
        fwrite($targetHandle, $targetContent);
    }

    protected function getMacros()
    {

    }
}
