<?php
/**
 * Created by PhpStorm.
 * User: Tarre
 * Date: 2018-07-06
 * Time: 12:27
 */
use Illuminate\Support\Str;

return [
    /*
    |--------------------------------------------------------------------------
    | General file settings
    |--------------------------------------------------------------------------
    |
    | Remember to change the namespace accordingly to the output_path!
    |
    */

    'output_path' => app_path('Repositories'),
    'repository_namespace' => 'App\Repositories',
    'template_path' => resource_path('laravel-repository-maker'),

    /*
    |--------------------------------------------------------------------------
    | Programming preferences
    |--------------------------------------------------------------------------
    |
    | People have different preferences when formatting method, variables and such, thus we allow the user to
    | Customize the output as much as possible before having to change the template.
    |
    */

    'names' => [
        'interface' => '%name%Repository',
        'class' => 'Eloquent%name%',
        'class_model_attribute' => '%name%',
        'controller' => '%name%Controller'
    ],

    // Formatter for parts of the templates
    'formatter' => [
        'method_variables' => function ($value) {
            return Str::studly($value);
        },
        'repository_names' => function ($value) {
            return Str::studly($value);
        },
        'controller_name' => function ($value) {
            return Str::studly($value);
        },
        'class_model_attribute' => function ($value) {
            return Str::studly($value);
        },
        'router_resource_name' => function ($value) {
            return Str::plural(mb_strtolower($value));
        }
    ],

    /*
    |--------------------------------------------------------------------------
    | Implementation, Extensions and Traits
    |--------------------------------------------------------------------------
    |
    | Define the default Traits for your repository classes
    | Example: [Full\Path\To\Some\Class:class]
    |
    */
    'uses' => [Illuminate\Database\Eloquent\Collection::class],

    'interface' => [
        'extends' => []
    ],

    'class' => [
        'extends' => [],
        'implements' => [],
        'uses' => [], // Traits
    ]
];
