<?php

namespace Tarre\RepositoryMaker;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;


class ServiceProvider extends BaseServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Publish necessary files
        $this->publishes([
            __DIR__ . DIRECTORY_SEPARATOR . 'Config' . DIRECTORY_SEPARATOR . 'laravel-repository-maker.php' => config_path('laravel-repository-maker.php'),
            __DIR__ . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'interface.template' => resource_path('laravel-repository-maker' . DIRECTORY_SEPARATOR . 'interface.template'),
            __DIR__ . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'class.template' => resource_path('laravel-repository-maker' . DIRECTORY_SEPARATOR . 'class.template'),
            __DIR__ . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'controller.template' => resource_path('laravel-repository-maker' . DIRECTORY_SEPARATOR . 'controller.template')
        ], 'laravel-repository-maker');

    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('command.make.repo', Console\MakeRepo::class);
        $this->commands('command.make.repo');
    }
}
