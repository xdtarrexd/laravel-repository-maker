## Was is das
Repository pattern made easy.

## Installation
1. `composer require tarre/laravel-repository-maker --dev`
2. `php artisan vendor:publish --tag=laravel-repository-maker --force`
3. Open `app/Providers/AppServiceProvider.php` do step 4 and 5.
4. Wrap `\\ USES-START` and `\\USES-END` around the `use Illuminate\Support\ServiceProvider` (and or) other `use`-keywords 
5. Add `\\ USES-START` and `\\USES-END` under anywhere in the register method.


#### Example of end result
```PHP
<?php

namespace App\Providers;

// USES-START
use Illuminate\Support\ServiceProvider;
// USES-STOP

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // REGISTER-START
        // REGISTER-STOP
    }
}
```

## How to use
`php artisan make:repository <name> <modelName?>`

**Switches**
* `--controller` creates an associated controller with the repository.
* `--route=web/api` creates an associated `route::Resource` entry.

**FYI**
if `<modelName>` is not given, a plain template will be used. if you want to create an associated controller: use the `--controller` switch


## FAQ

**Q:** Nothing happends when i try to run the command

**A:** Then your version of laravel probably does not support package discovery, add `Tarre\RepositoryMaker\ServiceProvider::class` to your `config\app.php`

**Q:** I want to change X / Where can i change the templates?

**A:** Change that settings in `config\laravel-repository-maker.php`. Templates are located in `resources\laravel-orm-maker` and can be modified freely.
